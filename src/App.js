import React from 'react';
import {Route, Switch} from "react-router-dom";
import { FileMonitor } from './views/FileMonitor';

function App() {
  return (
      <div>
        <Switch>
          <Route exact path="/">
            <FileMonitor />
          </Route>
        </Switch>
      </div>

  );
}

export default App;
