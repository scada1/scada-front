import React from 'react';
import { FileCard } from './components';
import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from "react-redux";
import {getDataByDay} from '../../redux/actions/fileMonitoring';
const useStyles = makeStyles( () => ({
  fileCardContainer: {
    textAlign: 'center',
  }
})
);

const FileMonitor = () => {
  const classes = useStyles();
  //const data = useSelector(state => state.fileMonitoring.data);
  const data = { filename: 'SF_20200548438', importPercentage: '83%', matchPercentage: '99%', status: 'success'}
  const dispatch = useDispatch();
  return (
    <div>
      <Button onClick={ () => {dispatch(getDataByDay('2020-05-27'))}}>Click me</Button>
      <Grid container spacing={4}>
        <Grid className={classes.fileCardContainer} item lg={4} sm={4} xl={4} xs={12}>
          <FileCard type='SAFE' data={data}/>
        </Grid>
        <Grid item className={classes.fileCardContainer} lg={4} sm={4} xl={4} xs={12}>
          <FileCard type='TC40' data={data}/>
        </Grid>
        <Grid item className={classes.fileCardContainer} lg={4} sm={4} xl={4} xs={12}>
          <FileCard type='NXS' data={data} />
        </Grid>

      </Grid>
    </div>
  );
};


export default FileMonitor;
