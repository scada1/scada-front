import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  Avatar,
  LinearProgress
} from '@material-ui/core';
import InsertChartIcon from '@material-ui/icons/InsertChartOutlined';

const useStyles = makeStyles(theme => ({
  title: {
    fontWeight: 700,
    color: theme.palette.success.contrastText
  },
  success: {
    backgroundColor: theme.palette.success.main
  }
}));


const FileCard = props => {
  const { type, data } = props;

  const classes = useStyles();

  return (
    <Card className={classes[data.status]}>
      <CardContent>
        <Grid container justify="space-between">
        </Grid>
      </CardContent>
    </Card>
  )
};

export default FileCard;
