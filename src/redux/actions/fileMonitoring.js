import { GET_DATA_BY_DAY, SET_DATA_BY_DAY } from '../types/fileMonitoring';
import { apiAction } from '../actions/api';

export const getDataByDay = date => apiAction({
        url: `http://localhost:8080/scada/data/${date}`,
        onSuccess: setData,
        onFailure: () => { console.log('Error while fetching user')},
        label: GET_DATA_BY_DAY
    });

const setData = data => ({
  type: SET_DATA_BY_DAY,
  payload: data
});
