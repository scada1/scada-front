import { combineReducers } from 'redux';
import fileMonitoring from './fileMonitoring';
/**
 * reducer example
const initialState = {
    current: 0,
};

export default function counter(state = initialState, action) {
    switch (action.type) {
        case INCREMENT:
            return {
                current: state.current += action.value,
            };

        case DECREMENT:
            return {
                current: state.current -= action.value,
            };

        default:
            return state;
    }
}
**/

const reducers = combineReducers({
  fileMonitoring: fileMonitoring
});

export default reducers;
