import { GET_DATA_BY_DAY, SET_DATA_BY_DAY } from '../types/fileMonitoring';

const initialState = {
  data: {
      date: '',
      safe: {},
      tc40: {},
      nxs: {}
  }
};

const fileMonitoring = (state = initialState, action) => {
    switch (action.type) {
        case SET_DATA_BY_DAY:

            return {data: orderData(action.payload.data)}
        default:
            return state;
    }
}

const orderData = data => {
    let dataOrdered = {
        date: data.date,
        safe: { receipts: [], imports: [], matchs: []},
        tc40: { receipts: [], imports: [], matchs: []},
        nxs: { receipts: [], imports: [], matchs: []}
    };

    dataOrdered.safe.receipts = data.receipts.filter( value => value.type === 'SAFE');
    dataOrdered.safe.imports = data.imports.filter( value => value.type === 'SAFE');
    dataOrdered.safe.matchs = data.matchs.filter( value => value.type === 'SAFE');

    dataOrdered.tc40.receipts = data.receipts.filter( value => value.type === 'TC40');
    dataOrdered.tc40.imports = data.imports.filter( value => value.type === 'TC40');
    dataOrdered.tc40.matchs = data.matchs.filter( value => value.type === 'TC40');

    dataOrdered.nxs.receipts = data.receipts.filter( value => value.type === 'NXS');
    dataOrdered.nxs.imports = data.imports.filter( value => value.type === 'NXS');
    dataOrdered.nxs.matchs = data.matchs.filter( value => value.type === 'NXS');

    return dataOrdered;
};

export default fileMonitoring;
